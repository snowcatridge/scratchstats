### About
This is a free tool that uses the Scratch API to create a list of more than 50 statistics based on Scratch. These statistics range from user-based statistics (such as how much followers a user has), to global statistics (such as who has the most followers).
